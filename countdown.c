///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
// Reference time: Sat Feb  6 00:00:00 2021
//
//Years: 0        Days: 2 Hours: 22       Minutes: 9      Seconds: 57
//Years: 0        Days: 2 Hours: 22       Minutes: 9      Seconds: 58
//Years: 0        Days: 2 Hours: 22       Minutes: 9      Seconds: 59
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 0
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 1
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 2
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 3
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 4
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 5
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 6
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 7
//Years: 0        Days: 2 Hours: 22       Minutes: 10     Seconds: 8
//   
//
// @author Brooke Maeda <bmhm@hawaii.edu>
// @date   4 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

struct tm refTime;



int main() {
   
   
   long diffsec, diffyear, diffday, diffhour, diffmin;
   time_t ref;
   time_t current_t;
   //set reference time
   refTime.tm_year = 2021 - 1900;
   refTime.tm_mon = 1;
   refTime.tm_mday = 6;
   refTime.tm_min = 0;
   refTime.tm_sec = 0;
   
   ref = mktime(&refTime);

   printf("Reference time: %s\n", asctime(&refTime));
   //time now
 
   mktime(&refTime);
   while(1){
      current_t = time(NULL);
     
      diffsec = difftime(ref, current_t);
      if(diffsec < 0)
         diffsec = diffsec*(-1);
      //calculate differences
      diffyear = diffsec/31536000;
      diffsec -= diffyear*31536000;
      diffday = diffsec/86400;
      diffsec -= diffday*86400;
      diffhour = diffsec/3600;
      diffsec -= diffhour*3600;
      diffmin = diffsec/60;
      diffsec -= diffmin*60;


      printf("Years: %ld\tDays: %ld\tHours: %ld\tMinutes: %ld\tSeconds: %ld\n",diffyear, diffday, diffhour, diffmin, diffsec);
      sleep(1);

   }


   return 0;
}


